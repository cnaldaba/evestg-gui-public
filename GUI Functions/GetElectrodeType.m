function [ electrode , errorMsg] = GetElectrodeType( handles )


pre2014 = get(handles.recordingDate_list, 'Value');
electrodeType = get(handles.electrode_list, 'Value');
site = get(handles.Sites_list, 'Value');

site_list = get(handles.Sites_list, 'String');
% Site list:
% 1 - Alfred
% 2 - Clayton
% 3 - St Kilda Rd
% 4 - RHC
% 5 - HSC

electrode_list = get(handles.electrode_list, 'String');
% Electrode type list:
% 1 - Cotton-wool
% 2 - Needle
% 3 - Biopac
% 4 - Prototype
% 5 - Mix

date_list = get(handles.recordingDate_list, 'String');

errorMsg = '';
electrode = 0;

switch(get(handles.Powerline_RdioBtn, 'SelectedObject'))
    case handles.PowerLine_50
        if ((site == 4) || (site == 5)) %Canadian sites
            errorMsg =sprintf('Invalid site and powerline combo! ( %s and 50 Hz )', site_list{site}) ;
            return
        end
    case handles.PowerLine_60
         if (~ ((site == 4) || (site == 5))) %Canadian sites
            errorMsg = sprintf('Invalid site and powerline combo! ( %s and 60 Hz )', site_list{site}) ;
            return
        end
end

if (pre2014 == 1) % Pre Sept 2014
    
    %Determine electrode based on site and electrode type
    if((site == 1) && (electrodeType == 3)) % Alfred and Biopac
        electrode = -1;
    elseif ((site == 1) && (electrodeType == 1)) % Alfred and Cotton-wool
        electrode = -9;
    elseif ((site == 2) && (electrodeType == 3)) % Clayton and Biopac
        electrode = -3;
    elseif ((site == 3) && (electrodeType == 1)) % St Kilda Rd and Cotton
        electrode = -9;
    elseif ((site == 4) && (electrodeType == 3)) % RHC and Biopac
        electrode = -7;
    elseif ((site == 4) && (electrodeType == 4)) % RHC and Prototype
        electrode = -11;
    elseif ((site == 4) && (electrodeType == 1)) % RHC and Cotton-wool
        electrode = -5;
    elseif ((site == 4) && (electrodeType == 5)) % RHC and Mix (TM,TM, biopac8)
        electrode = -14;
    elseif ((site == 5) && (electrodeType == 2)) % HSC and Needle
        electrode = -13;
    else
        errorMsg = sprintf('Invalid combination for Recording date: %s, Site:%s, Electrode:%s.',...
            date_list{pre2014}, site_list{site}, electrode_list{electrodeType});
    end
    
else % Post Sept 2014
    
    %Determine electrode based on site and electrode type
    if((site == 1) && (electrodeType == 3)) % Alfred and Biopac
        electrode = -2;
    elseif ((site == 1) && (electrodeType == 4)) % Alfred and Prototype
        electrode = -12;
    elseif ((site == 1) && (electrodeType == 1)) % Alfred and Cotton-wool
        electrode = -10;
    elseif ((site == 2) && (electrodeType == 3)) % Clayton and Biopac
        electrode = -4;
    elseif ((site == 3) && (electrodeType == 4)) % St Kilda Rd and Prototype
        electrode = -12;
    elseif ((site == 3) && (electrodeType == 1)) % St Kilda Rd and Cotton-wool
        electrode = -10;
    elseif ((site == 4) && (electrodeType == 3)) % RHC and Biopac
        electrode = -8;
    elseif ((site == 4) && (electrodeType == 1)) % RHC and Cotton-wool
        electrode = -6;
    else
        errorMsg = sprintf('Invalid combination for Recording date: %s, Site:%s, Electrode:%s.',...
            date_list{pre2014}, site_list{site}, electrode_list{electrodeType});
    end
    
end

end

