function dialogObj = BusyDialog(message, title)

dialogObj = dialog('Name',title);
handles_dialogObj = get(dialogObj);
pos = handles_dialogObj.Position;
pos(3) = 250;
pos(4) = 150;

delete(dialogObj);

dialogObj = dialog('Name',title, 'Position', pos);
txt = uicontrol('Parent',dialogObj,...
    'Style','text',...
    'Position',[20 80 210 40],...
    'String',message);

% Getting BusyAffordance i.e. busy spinning icon 
% Source: https://undocumentedmatlab.com/blog/animated-busy-spinning-icon
try
    % R2010a and newer
    iconsClassName = 'com.mathworks.widgets.BusyAffordance$AffordanceSize';
    iconsSizeEnums = javaMethod('values',iconsClassName);
    SIZE_32x32 = iconsSizeEnums(2);  % (1) = 16x16,  (2) = 32x32
    jObj = com.mathworks.widgets.BusyAffordance(SIZE_32x32, 'Busy...');  % icon, label
catch
    % R2009b and earlier
    redColor   = java.awt.Color(1,0,0);
    blackColor = java.awt.Color(0,0,0);
    jObj = com.mathworks.widgets.BusyAffordance(redColor, blackColor);
end
javacomponent(jObj.getComponent, [80,20,80,80], dialogObj);
jObj.start;
end