function [ rootFile ] = GetEVestGRootFileName( fileName )
%GETEVESTGROOTFILENAME Extracts EVestG root file name
%Allows user to select any *.mat or *.smr file and program
%can look for any specific filename that the NEER algorithm needs to
%open or create
   
rootFile = '';

evestg_postfix = { 'bt', 'pronerot','rot','side', 'proneup', 'prone',... % SMR Files
    'ctrightCD', 'ctleftCD', 'itrightCD', 'itleftCD', ... % SMR Files
    'btrightCD', 'btleftCD', 'rotrightCD','rotleftCD', 'uprightCD', ... % MAT files
    'upleftCD', 'proneuprightCD','proneupleftCD', 'pronerotrightCD','prontrotleftCD' % MAT files
    }; %What pattern of characters we expect to find in an EVestG file

for i=1:length(evestg_postfix)
    startIndex = cell2mat(regexp(fileName, evestg_postfix(i))); %Looks for pattern
    if(startIndex > 0)
        rootFile = fileName(1:startIndex-1); %Extracts root file name
        break;
    end
end
end

