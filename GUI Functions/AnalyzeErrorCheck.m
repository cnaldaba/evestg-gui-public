function [ evestg_params, errorMsg ] = AnalyzeErrorCheck( handles )
%ANALYZEERRORCHECK Summary of this function goes here
%   Detailed explanation goes here

errorMsg = '';

if( ~isNumber(get(handles.preFiltTxt,'String')))
    errorMsg{end+1} =  'Pre-Filter is NaN';
    evestg_params =  struct([]);
    return
else
    evestg_params.preFilt =  str2double(get(handles.preFiltTxt,'String'));
end

if( ~isNumber(get(handles.gainTxt,'String')))
    errorMsg{end+1} =  'Gain is NaN';
    evestg_params =  struct([]);
    return
else
    evestg_params.gain =  str2double(get(handles.gainTxt,'String'));
end

if( ~isNumber(get(handles.HPFTxt,'String')))
    errorMsg{end+1}= 'HPF Frequency is NaN';
    evestg_params =  struct([]);
    return
else
    evestg_params.HPF =  str2double(get(handles.HPFTxt,'String'));
end

if( ~isNumber(get(handles.LPFTxt,'String')))
    errorMsg{end+1}=  'LPF Frequency is NaN';
    evestg_params =  struct([]);
    return
else
    evestg_params.LPF =  str2double(get(handles.LPFTxt,'String'));
end

%% Get everything else because it seems like everything is ok

switch(get(handles.NumFilts_RdioBtn, 'SelectedObject'))
    case handles.NumFilts_1:
        evestg_params.numFilts = 1;
    case handles.NumFilts_2:
        evestg_params.numFilts = 2;
    case handles.NumFilts_3:
        evestg_params.numFilts = 3;
end


end

