function [errorMsg ] = eVestGRecordingInfoErrorCheck( handles,  evestg_params )

errorMsg = '';
date_list = get(handles.recordingDate_list, 'String');
pre2014 = get(handles.recordingDate_list, 'Value');

electrode_list = get(handles.electrode_list, 'String');
electrodeType = get(handles.electrode_list, 'Value');

site_list = get(handles.Sites_list, 'String');
site = get(handles.Sites_list, 'Value');

% Site list:
% 1 - Alfred
% 2 - Clayton
% 3 - St Kilda Rd
% 4 - RHC
% 5 - HSC

% Electrode type list:
% 1 - Cotton-wool
% 2 - Needle
% 3 - Biopac
% 4 - Prototype
% 5 - Mix

if(pre2014)
    invalid = ( (site == 5) && (electrodeType ~= 2)) || ( (site ~= 5) && (electrodeType == 2) ...
        || ((site ~= 4)  && (electrodeType == 4)) || ((site ~= 4)  && (electrodeType == 5)) ...
        || ( (site == 2) && (electrodeType == 1));
    if (invalid)
        errorMsg = sprintf('Invalid combination for Recording date: %s, Site:%s, Electrode:%s.',...
            date_list{pre2014}, site_list{site}, electrode_list{electrodeType});
    end
    %     valid = ((site ~= 5) && (electrodeType == 3)) || (site == 4) || ( (site == 5 ) &&(electrodeType ==2));
    %     if(~valid)
    %         errorMsg = sprintf('Invalid combination for Recording date: %s, Site:%s, Electrode:%s.',...
    %             date_list{pre2014}, site_list{site}, electrode_list{electrodeType});
    %     end
else
    invalid = (electrodeType == 5) || (electrodeType == 2) || (site == 5) ...
        || ((site == 2) && (electrodeType == 4)) ...
        || ((site == 2) && (electrodeType == 1)) ...
        || ((site == 3) && (electrodeType == 3)) ...
        || ((site == 4) && (electrodeType == 4)) ;
    
     if (invalid)
        errorMsg = sprintf('Invalid combination for Recording date: %s, Site:%s, Electrode:%s.',...
            date_list{pre2014}, site_list{site}, electrode_list{electrodeType});
    end
end



end

