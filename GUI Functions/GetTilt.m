function [ tilt, errorMsg ] = GetTilt( handles )
%GETTILT Summary of this function goes here
%   Detailed explanation goes here
errorMsg = '';

tiltList = get(handles.Tilts_list, 'String');
selected = get(handles.Tilts_list,'Value');

tiltStr = tiltList(selected);
invalid = ((get(handles.NewAlfred_RdioBtn, 'SelectedObject') == handles.NewAlfred_Y) &&...
    strcmp(tiltStr, 'Prone rotation')) || ...
    ((get(handles.NewAlfred_RdioBtn, 'SelectedObject') == handles.NewAlfred_Y) &&...
    strcmp(tiltStr, 'Prone Up'));
tilt = 0;
if(invalid)
    errorMsg = 'INVALID - New system with selected tilts!';
    tilt = 0;
else
    if(strcmp(tiltStr,'Side only'))
        tilt = 1;
    elseif (strcmp(tiltStr,'Rot, proneup (supineup)'))
        tilt = -1;
    elseif(strcmp(tiltStr,'All tilts'))
        tilt = -2;
    elseif(strcmp(tiltStr,'All but prone/supine'))
        tilt = -3;
    elseif(strcmp(tiltStr,'Proneup or supine up only'))
        tilt = -4;
    elseif(strcmp(tiltStr, 'Supinerot only'))
        tilt = -5;
    elseif(strcmp(tiltStr, 'Supinerot and supineup'))
        tilt = -6;
    elseif(strcmp(tiltStr, 'Bt only'))
        tilt = -7;
    end
    
    
end



end

