function [ evestg_params, errorMsg ] = GetEvestGParameters( handles )
%ANALYZEERRORCHECK Summary of this function goes here
%   Detailed explanation goes here

errorMsg = '';



if( ~isNumber(get(handles.gainTxt,'String')))
    errorMsg{end+1} =  'Gain is NaN';
    evestg_params =  struct([]);
    return
else
    evestg_params.gain =  str2double(get(handles.gainTxt,'String'));
end

if( ~isNumber(get(handles.HPFTxt,'String')))
    errorMsg{end+1}= 'HPF Frequency is NaN';
    evestg_params =  struct([]);
    return
else
    evestg_params.HPF =  str2double(get(handles.HPFTxt,'String'));
end

if( ~isNumber(get(handles.LPFTxt,'String')))
    errorMsg{end+1}=  'LPF Frequency is NaN';
    evestg_params =  struct([]);
    return
else
    evestg_params.LPF =  str2double(get(handles.LPFTxt,'String'));
end

if( ~isNumber(get(handles.HydJitFreqTxt,'String')))
    errorMsg{end+1}=  'HydJitFreq Frequency is NaN';
    evestg_params =  struct([]);
    return
else
    evestg_params.HydJitFreq =  str2double(get(handles.HydJitFreqTxt,'String'));
end

%% Get everything else because it seems like everything is ok
switch(get(handles.preFilter, 'SelectedObject'))
    case handles.preFilter_Y
        evestg_params.prefilter = 1;
    case handles.preFilter_N
        evestg_params.prefilter = 0;
end


switch(get(handles.NumFilts_RdioBtn, 'SelectedObject'))
    case handles.NumFilts_1
        evestg_params.numFilts = 1;
    case handles.NumFilts_2
        evestg_params.numFilts = 2;
    case handles.NumFilts_3
        evestg_params.numFilts = 3;
end

switch(get(handles.Powerline_RdioBtn, 'SelectedObject'))
    case handles.PowerLine_50
        evestg_params.powerLine = 50;
    case handles.PowerLine_60
        evestg_params.powerLine = 60;
end

switch(get(handles.WhiteNoise_RdioBtn, 'SelectedObject'))
    case handles.WhiteNoise_Y
        evestg_params.whiteNoise = true;
    case handles.WhiteNoise_N
        evestg_params.whiteNoise = false;
end

switch(get(handles.NewAlfred_RdioBtn, 'SelectedObject'))
    case handles.NewAlfred_Y
        evestg_params.newAlfred = true;
    case handles.NewAlfred_N
        evestg_params.newAlfred = false;
end

switch(get(handles.FileType_RdioBtn, 'SelectedObject'))
    case handles.FileType_MAT
        evestg_params.MATorSMR = true;
    case handles.FileType_SMR
        evestg_params.MATorSMR = false;
end


end

