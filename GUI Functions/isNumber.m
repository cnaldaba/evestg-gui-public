function [ output ] = isNumber( input )
%ISNUMBER Check if string is a number
%   Detailed explanation goes here

output = all(ismember(input,'0123456789+-.'));

end

