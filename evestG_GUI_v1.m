function varargout = evestG_GUI_v1(varargin)
% EVESTG_GUI_V1 MATLAB code for evestG_GUI_v1.fig
%      EVESTG_GUI_V1, by itself, creates a new EVESTG_GUI_V1 or raises the existing
%      singleton*.
%
%      H = EVESTG_GUI_V1 returns the handle to a new EVESTG_GUI_V1 or the handle to
%      the existing singleton*.
%
%      EVESTG_GUI_V1('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in EVESTG_GUI_V1.M with the given input arguments.
%
%      EVESTG_GUI_V1('Property','Value',...) creates a new EVESTG_GUI_V1 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before evestG_GUI_v1_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to evestG_GUI_v1_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help evestG_GUI_v1

% Last Modified by GUIDE v2.5 29-Oct-2018 13:00:42

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @evestG_GUI_v1_OpeningFcn, ...
    'gui_OutputFcn',  @evestG_GUI_v1_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before evestG_GUI_v1 is made visible.
function evestG_GUI_v1_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to evestG_GUI_v1 (see VARARGIN)

% Choose default command line output for evestG_GUI_v1
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes evestG_GUI_v1 wait for user response (see UIRESUME)
% uiwait(handles.figure1);
initializeParameters(handles);


% --- Outputs from this function are returned to the command line.
function varargout = evestG_GUI_v1_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


function initializeParameters(handles)

set(handles.HPFTxt, 'String', 600);
set(handles.gainTxt, 'String',1);
set(handles.LPFTxt, 'String',11000);
set(handles.HydJitFreqTxt, 'String',980);

sites = {'Alfred (Baker Inst.)','Clayton','St. Kilda Rd','RHC','HSC'};
set(handles.Sites_list, 'String',sites);

electrodes = {'Cotton-wool','Needle','Biopac','Prototype','Mix'};
set(handles.electrode_list, 'String', electrodes);

dates = {'Pre Sept. 2014','Post Sept. 2014'};
set(handles.recordingDate_list, 'String', dates);

tilts = {'Side only','Rot, proneup (supineup)','All tilts','All but prone/supine',...
    'Proneup or supine up only','Supinerot only','Supinerot and supineup','Bt only'};
set(handles.Tilts_list,'String', tilts);


set( handles.NewAlfred_RdioBtn, 'SelectedObject',handles.NewAlfred_N);
set(handles.WhiteNoise_RdioBtn,'SelectedObject',handles.WhiteNoise_Y);
set(handles.Powerline_RdioBtn,'SelectedObject',handles.PowerLine_60);
set(handles.NumFilts_RdioBtn,'SelectedObject',handles.NumFilts_2);

%Include sub-folders that contain the original evestg code
addpath(genpath(pwd));

set(handles.analyzeBtn, 'Enable','Off');


I = imread('evestgLogo.png');
axes(handles.Logo);
imshow(I);



% --- Executes on button press in analyzeBtn.
function analyzeBtn_Callback(hObject, eventdata, handles)
% hObject    handle to analyzeBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get eVestG Parameters
[evestg, error] = GetEvestGParameters(handles);
if (~strcmp(error,''))
    errordlg(error, 'eVestG Analyze Error');
    return
end

[tilt, error] = GetTilt(handles);
if (~strcmp(error,''))
    errordlg(error, 'eVestG Analyze Error');
    return
else
    evestg.tilts2read = tilt;
end


[electrode, error] = GetElectrodeType(handles);

if (~strcmp(error,''))
    errordlg(error, 'eVestG Analyze Error');
    return
else
    evestg.electrode = electrode;
end


if (~evestg.newAlfred && evestg.MATorSMR)
    errordlg('Old Alfred system does not have MAT files!', 'eVestG Analyze Error');
    return
end


%Get list of selected files 
fileList = get(handles.Files_List,'String');
selectedFiles = get(handles.Files_List,'Value');

%Disable analyze button and open busy dialog
set(handles.analyzeBtn, 'Enable','Off');
busyDialogBox = BusyDialog('Executing NEER Algorithm...','EVestG');
pause on
pause (0.5);
pause off
for ii=1:length(selectedFiles)
    
    %Get iith selected file from list
    if(iscell(fileList))
        filePath = fileList{ii};
        [path,name,ext] = fileparts(filePath);
    else
        filePath = fileList;
        [path,name,ext] = fileparts(fileList);
    end
   
    dispStr = sprintf('Analyzing file: %s', name);
    disp(dispStr);
    
    rootFileName = GetEVestGRootFileName(name);
    rootPath = fullfile(path,rootFileName);
    
    if(evestg.newAlfred) %NEER Algorithm starts
        disp('************NEERSegmentBLV02_300onlyNewSys************')
        NEERSegmentBLV02_300onlyNewSys(evestg.MATorSMR ,...
            evestg.powerLine,...
            evestg.tilts2read, ...
            rootPath,...
            evestg.gain,...
            evestg.prefilter, ...
            evestg.numFilts,...
            evestg.HPF,...
            evestg.LPF);
        
        %NEER  to produce EVG plots and intervals--36 plots:
        spap_8_8_11_300onlyNewSys(evestg.powerLine,evestg.tilts2read,rootPath, evestg.whiteNoise);
        disp('************spap_8_8_11_300onlyNewSys************')
        spapUp_8_8_11_300onlyNewSys(evestg.powerLine,evestg.tilts2read,rootPath, evestg.whiteNoise);
        disp('************spapUp_8_8_11_300onlyNewSys************')
        %%%%%%%%%%%%DiagnoseInterval290409(analysedfile); %determine intervals
        Interval_9_9_11_300onlyNewSys(evestg.tilts2read,rootPath);
        disp('************Interval_9_9_11_300onlyNewSys************')
    else
        disp('************NEERSegmentBLV02_300only************');
        NEERSegment2018(...
            evestg.powerLine,...
            evestg.tilts2read,...
            rootPath,...
            evestg.gain,...
            evestg.prefilter,...
            evestg.numFilts,...
            evestg.HPF,...
            evestg.LPF,...
            evestg.HydJitFreq);%SMR only, LPF=11kHz
        
        disp('NEERSegmentBLV02_300only')
        %NEER  to prodce EVG plots and intervals--36 plots
        spap_sept14(evestg.powerLine,evestg.tilts2read,rootPath,evestg.whiteNoise,evestg.electrode);
        disp('************spap_sept14************')
        spapUp_sept14(evestg.powerLine,evestg.tilts2read,rootPath,evestg.whiteNoise,evestg.electrode);
        disp('************spapUp_sept14************')
        %%%%%%%%%%%%DiagnoseInterval290409(analysedfile); %determine intervals
        Interval_1to20(evestg.tilts2read,rootPath);
        disp('************Interval_1to20************')
    end
    pause on;
    pause (20);% 60 sec delay to allow for excel to catch up in Matlab2013b?
    pause off;
    
end

delete(busyDialogBox); % NEER Algorithm is completed for all selected files
f = msgbox('Operation Completed!','EVestG'); %Show msg box
set(handles.analyzeBtn, 'Enable','On'); %Re-enable analyze button



% --- Executes on button press in browseBtn.
function browseBtn_Callback(hObject, eventdata, handles)
% hObject    handle to browseBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

waitfor(msgbox('Select one .smr or .mat file per patient', 'Add files...'));

[file, path] = uigetfile('*.mat;*.smr', 'MultiSelect', 'on');
if( ischar(file) || iscell(file))
    newFiles = fullfile(path,file);
    currentFileList = get(handles.Files_List,'String');
    
    if(ischar(newFiles)) %If adding one file
        currentFileList{end+1} = newFiles;
    else%If adding multiple files
        for i=1:length(newFiles) 
            currentFileList{end+1} = newFiles{i}
        end
    end

    set(handles.Files_List, 'String', currentFileList);
    set(handles.analyzeBtn, 'Enable','On');
elseif (file == 0)
    errordlg('No file selected!', 'EVestG File Error');
    set(handles.analyzeBtn, 'Enable','Off');
end


% --- Executes on button press in HelpBtn.
function HelpBtn_Callback(hObject, eventdata, handles)
% hObject    handle to HelpBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

I = imread('eVestGSettingTable.jpg');

G = figure('menubar', 'none','Name', 'EVestG - Valid Electrode Settings Help', ...
    'NumberTitle', 'off',...
    'Toolbar', 'none');

imshow(I);


% --- Executes on button press in removeBtn.
function removeBtn_Callback(hObject, eventdata, handles)
% hObject    handle to removeBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
fileList = get(handles.Files_List,'String');

if(length(fileList) >0)
    waitfor(msgbox('Select one or more files to remove', 'Remove files...'));
    
    [indx,~] = listdlg('ListString',fileList,...
        'PromptString','Select a file(s) to remove:',...
        'SelectionMode','multiple',...
        'ListSize', [450 200]);
    
    copyList = fileList;
    for i = 1:length(indx)
        toRemoveIndx = find( strcmp( fileList,copyList(indx(i)) ) );
        fileList(toRemoveIndx) = [];
    end
    
    set(handles.Files_List, 'String',fileList);
else
    errordlg('No files to remove!', 'EVestG File Error');
end





%CreateFcns (unused) that matlabs needs or else matlab will pop-up errors
function HPFTxt_CreateFcn(hObject, eventdata, handles)
function gainTxt_CreateFcn(hObject, eventdata, handles)
function LPFTxt_CreateFcn(hObject, eventdata, handles)
function popupmenu2_CreateFcn(hObject, eventdata, handles)
function Sites_list_CreateFcn(hObject, eventdata, handles)
function preFiltTxt_CreateFcn(hObject, eventdata, handles)
function HydJitFreqTxt_CreateFcn(hObject, eventdata, handles)
function HydJitFreqTxt_Callback(hObject, eventdata, handles)

%SelectionChangedFcns for radio buttons. If not included, GUI will always
%revert these parameters to initial values
function preFilter_SelectionChangedFcn(hObject, eventdata, handles)
function Powerline_RdioBtn_SelectionChangedFcn(hObject, eventdata, handles)
function NumFilts_RdioBtn_SelectionChangedFcn(hObject, eventdata, handles)
function WhiteNoise_RdioBtn_SelectionChangedFcn(hObject, eventdata, handles)
function NewAlfred_RdioBtn_SelectionChangedFcn(hObject, eventdata, handles)
function FileType_RdioBtn_SelectionChangedFcn(hObject, eventdata, handles)


function Files_List_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Files_List (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function Files_List_Callback(hObject, eventdata, handles)
% --- Executes during object creation, after setting all properties.






